
/*global define */
define(["backbone", "underscore", "jquery", "text!templates/section-template.html"], function (Backbone, _, $, tmpl) {
  var consts = {
    className: "row section-row",
    keyframeClass: "keyframe-col",
    thumbClass: "section-thumbnail",
    summaryDivClass: "abs-summary"
  };

  return Backbone.View.extend({
    template: _.template(tmpl),
    id: function () {
      return this.model.cid;
    },
    className: consts.className,

    events: {
      'keyup .abs-summary': "summaryKeyUp",
      "click .remove-section": "removeSection",
      "click .take-thumbnail-image": "takeThumbnailImage",
      "click .keyframe-col": "startVideo",
      "click .abs-summary": "clickSummary"
    },

    initialize: function () {
      var thisView = this,
          thisModel = thisView.model;

      // listen for thumbnail changes
      thisView.listenTo(thisModel, "change:thumbnail", function (mdl) {
        var $img = $("<img>");
        $img.addClass(consts.thumbClass);
        $img.attr("src", mdl.get("thumbnail").get("data"));
        thisView.$el.find("." + consts.keyframeClass).html($img);
      });

      thisView.listenTo(thisModel, "change:summary", function (mdl, val) {
        thisView.$el.find("." + consts.summaryDivClass).html(val);
      });

      thisView.listenTo(thisModel, "gainfocus", function (mdl, val) {
        window.setTimeout(function () {
          thisView.$el.find("." + consts.summaryDivClass)[0].focus();
        }, 200);
      });

      // remove the view if the underlying model is removed
      thisView.listenTo(thisModel, "remove", function (mdl) {
        thisView.remove();
      });
    },

    render: function () {
      var thisView = this;
      thisView.$el.html(thisView.template(thisView.model.toJSON()));
      return thisView;
    },

    summaryKeyUp: function (evt) {
      var thisView = this,
      $curTar = $(evt.currentTarget),
      text = $curTar.text();
      if (text !== thisView.model.get("summary")) {
        thisView.model.set("summary", text);
        // USE STATS
        window.vdstats.nSummaryEdits.push((new Date()).getTime());
      }
    },

    removeSection: function (evt) {
      var thisView = this,
          thisModel = thisView.model;
      // TODO for now, make sure it's not the first section
      if (window.confirm("Are you sure you want to remove this section?")) {
        thisModel.get("startWord").set("startSection", false);
        console.log("section deleted");
      }
    },

    takeThumbnailImage: function (evt) {
      var thisView = this,
          thisModel = thisView.model;
      thisModel.trigger("captureThumbnail", thisModel);
      console.log("capturing thumbnail image.");

      // USE STATS
      window.vdstats.nKeyFrameChanges.push((new Date()).getTime());

    },

    startVideo: function () {
      var thisView= this,
          thisModel = thisView.model;
      thisModel.trigger("startVideo", thisModel.get("startWord").get("start"));
    },

    clickSummary: function () {
      var thisView = this,
          startWord = thisView.model.get("startWord");
      startWord.trigger("focus", startWord);
    }
  });
});
